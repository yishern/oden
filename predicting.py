import json 
import numpy as np 
import os 
import tensorflow as tf 

from luminoth.models import get_model 
from luminoth.datasets import get_dataset 

class RPNNetwork(object):

	def __init__(self, config):
		"""RPN - Region Proposal network 

		Given an image (as feature map) and a fixed set of anchors, the RPN 
		adjusts anchors to fit objects, as well as scoring them by the probability 
		of it being an object/background
		"""
		
		config.dataset.data_augmentation = None 
		dataset_class = get_dataset(config.dataset.type)
		model_class = get_model(config.model.type)
		dataset = dataset_class(config)
		model = model_class(config)

		graph = tf.Graph()
		self.session = tf.Session(graph=graph, config=tf.ConfigProto(device_count={'GPU': 1}))

		with graph.as_default():
			self.image_placeholder = tf.placeholder(
				tf.float32, (None, None, 3)
			)
			image_tf, _, process_meta = dataset.preprocess(
				self.image_placeholder
			)
			pred_dict = model(image_tf)

			# Restore checkpoint 
			if config.train.job_dir:
				job_dir = config.train.job_dir
				if config.train.run_name:
					job_dir = os.path.join(job_dir, config.train.run_name)
				ckpt = tf.train.get_checkpoint_state(job_dir)
				if not ckpt or not ckpt.all_model_checkpoint_paths:
					raise ValueError('Could not find checkpoint in {}'.format(
						job_dir
					))
				ckpt = ckpt.all_model_checkpoint_paths[-1]
				saver = tf.train.Saver(sharded=True, allow_empty=True)
				saver.restore(self.session, ckpt)
				tf.logging.info('Loaded checkpoint')

			assert config.model.type == 'fasterrcnn'
			assert not config.model.network.get('with_rcnn', False)

			rpn_prediction = pred_dict['rpn_prediction']
			objects_tf = rpn_prediction['proposals']
			objects_labels_prob_tf = rpn_prediction['scores']

			self.fetches = {
				'objects': objects_tf,
				'probs': objects_labels_prob_tf,
				'scale_factor': process_meta['scale_factor']
			}

	def predict_image(self, image):
		fetched = self.session.run(self.fetches, feed_dict={
			self.image_placeholder: image
		})

		objects = fetched['objects']
		probs = fetched['probs'].tolist()
		scale_factor = fetched['scale_factor']

		if isinstance(scale_factor, tuple):
			objects /= [scale_factor[1], scale_factor[0], scale_factor[1], scale_factor[0]]
		else:
			objects /= scale_factor 

		objects = [
			[int(round(coord)) for coord in obj]
			for obj in objects.tolist()
		]

		predictions = [
			{
				'bbox': obj,
				'prob': round(prob, 4)
			} for obj, prob in zip(objects, probs)
		]

		# If require sorting in future, use this instead
		# predictions = sorted([
		# 	{
		# 		'bbox': obj,
		# 		'prob': round(prob, 4)
		# 	} for obj, prob in zip(objects, probs)
		# ], key=lambda x: x['prob'], reverse=True)

		return predictions 

