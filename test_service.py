import requests 
import argparse 
from datetime import datetime 
from threading import Thread
import time 

def call_predict_endpoint(endpoint, n, url):
	start = datetime.now()
	r = requests.post(endpoint, data={'url': url})
	if r.status_code == requests.codes.ok:
		response = r.json()
		end = datetime.now()
		if response['success']:
			print('Thread {n} OK'.format(n=n+1))
			print('Number of detections: {num_detections}'.format(num_detections=len(response['detections'])))
		duration = end - start
		print('Duration of process: {}'.format(duration))
		return True
	else:
		print('Thread {n} REQUEST FAILED \nReason: {reason}'.format(n=n, reason=r.reason))
		return False 


if __name__ == '__main__':
	ap = argparse.ArgumentParser()
	ap.add_argument('--test_url', type=str, default='http://amz450.sea.123rf.com/assetsbucket0901/s/lisafx/lisafx0506/lisafx050600136.jpg')
	ap.add_argument('--api', type=str)
	ap.add_argument('--num_request', type=int, default=10)
	ap.add_argument('--sleep_count', type=float, default=0.05)
	ap.add_argument('--multithreaded', action='store_true', default=False)
	args = ap.parse_args()

	if args.multithreaded:
		for i in range(args.num_request):
			t = Thread(target=call_predict_endpoint, args=(args.api, i, args.test_url,))
			t.daemon=True 
			t.start()
			time.sleep(args.sleep_count)
	else:
		for i in range(args.num_request):
			call_predict_endpoint(args.api, i, args.test_url)

	print('Entering to sleep now while awaiting remaining response (if any)')
	time.sleep(300)
		