# Oden 

This is an object detection implementation based on the Region Proposal Network of the Faster-RCNN model. Hence, this implementation serves to detect objects only without recognizing their classes.

## Dependencies
Apache
```
yum install httpd
```
Redis-Server
```
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
sudo make install
yum install tcl 
yum install tcl-devel
```
mod_wsgi
```
yum install python36u-mod_wsgi
```

## Setup
1. Clone this repository
```git
git clone https://yishern@bitbucket.org/yishern/oden.git
```
2. Create virtualenv 
```
mkvirtualenv object_detection -p python3.6
workon object_detection
```
3. Install the required Python Libraries
```
cd oden
pip install -r requirements.txt 
```
Note: Replace `tensorflow` with `tensorflow-gpu` if your current setup has a GPU 

4. Edit the paths accordingly in `object_detect_microservice.wsgi`
5. Create a file `000-default.conf` in /etc/httpd/conf.d.
```
LoadModule wsgi_module modules/mod_wsgi.so
WSGIPythonPath /home/webteam/.virtualenvs/object_detection/lib/python3.6/site-packages

<VirtualHost *:80>
        WSGIDaemonProcess oden
        WSGIScriptAlias / /var/www/oden/object_detect_microservice.wsgi
        WSGIImportScript /var/www/oden/object_detect_microservice.wsgi \
                process-group=oden application-group=%{GLOBAL}

        <Directory /var/www/oden>
                Options +FollowSymLinks -SymLinksIfOwnerMatch
                WSGIProcessGroup oden
                WSGIApplicationGroup %{GLOBAL}
                Order deny,allow
                Allow from all
        </Directory>
</VirtualHost>
```
Note: Remember to change the path given in WSGIPythonPath accordingly.</p>
6. Copy `redis.conf` from the downloaded the downloaded Redis folder to this repository. Then on the section "Snapshotting", add `#` before all the lines that starts with `save` and add `save ""` below the lines.
```
################################ SNAPSHOTTING  ################################
#
# Save the DB on disk:
#
#   save <seconds> <changes>
#
#   Will save the DB if both the given number of seconds and the given
#   number of write operations against the DB occurred.
#
#   In the example below the behaviour will be to save:
#   after 900 sec (15 min) if at least 1 key changed
#   after 300 sec (5 min) if at least 10 keys changed
#   after 60 sec if at least 10000 keys changed
#
#   Note: you can disable saving completely by commenting out all "save" lines.
#
#   It is also possible to remove all the previously configured save
#   points by adding a save directive with a single empty string argument
#   like in the following example:
#
#   save ""

#save 900 1
#save 300 10
#save 60 10000
save ""
```
7. Create a folder `checkpoints/` within the repository and place the model's checkpoint under the `checkpoints/`
```bash
|-- oden
    |--checkpoints
        |--fastercnn_checkpoint
            |--checkpoint
            |--classes.json
            |--config.yml
            |--model.ckpt.data...
            |--model.ckpt-...index
            |--model.ckpt-...meta
    |--configs.py
    |--helpers.py
    ...
```
8. In `service_config.json`, edit the path in `model_config` to the full path of `config.yml` which contains all the architecture choices for the RPN Network

## Launch
Execute the following commands
```bash
redis-server /var/www/oden/redis.conf &
redis-cli FLUSHALL
sudo systemctl start httpd.service
<path_to_virtualenv>/bin/python3.6 /var/www/oden/run_model_server.py &
```
You can monitor the log of the microservice in `/var/log/httpd/error_log'

## Test 
```bash
curl -X POST http://localhost/detect -F url=<insert_an_image_URL>
```
Example:
```
curl -X POST http://localhost/detect -F url=https://dynl.mktgcdn.com/p/r0E3VIlsyZUwIfiQt_v2Tp_WvxeaE4dVbv2RavIL6PQ/551x450.png

{"detections":[{"bbox":[116,219,229,415],"prob":0.9999},{"bbox":[388,116,442,253],"prob":0.9995},{"bbox":[163,186,174,231],"prob":0.999},{"bbox":[183,295,529,416],"prob":0.9988},{"bbox":[27,121,91,235],"prob":0.9986},{"bbox":[176,189,184,228],"prob":0.9983},{"bbox":[184,191,194,230],"prob":0.9983},{"bbox":[269,168,299,258],"prob":0.9913},{"bbox":[34,247,106,263],"prob":0.9871},{"bbox":[395,233,419,260],"prob":0.9818},{"bbox":[201,232,505,297],"prob":0.9744},{"bbox":[367,189,407,229],"prob":0.9737},{"bbox":[286,248,322,258],"prob":0.9736},{"bbox":[79,221,117,229],"prob":0.9722},{"bbox":[322,291,399,319],"prob":0.9705},{"bbox":[483,220,528,351],"prob":0.9682},{"bbox":[241,166,265,227],"prob":0.9633},{"bbox":[327,239,359,263],"prob":0.9558},{"bbox":[380,221,392,249],"prob":0.9556},{"bbox":[424,287,496,311],"prob":0.9506}],"error_message":"","success":true}
```