import flask 
from configs import config 
import redis 
import requests 
from PIL import Image 
from io import BytesIO 
import numpy as np 
import uuid 
import helpers 
from configs import config 
import json 
import time  
import sys 


app = flask.Flask(__name__)
db = redis.StrictRedis(**config.redis)

def download_image(url):
	try:
		r = requests.head(url)
		if r.headers['content-type'] not in config.accepted_header_types:
			return None 
		response = requests.get(url, timeout=(config.request_timeout.connect,config.request_timeout.read))
		if response.status_code != requests.codes.OK:
			return None 
		img = Image.open(BytesIO(response.content)).convert('RGB')
		img = np.array(img)
		return img 
	except Exception as e: 
		print(e)
		return None

@app.route('/')
def homepage():
	return "Welcome to Object Detection API"

@app.route('/detect', methods=['POST'])
def predict():
	data = {'success': False, 'error_message': ''}
	if flask.request.method == 'POST':
		try:
			# Download posted URL and convert to NumPy array
			url = flask.request.form.get('url')
			# print(url)
			image = download_image(url)
			if image is None:
				data['error_message'] = 'URL download failed'
				return flask.jsonify(data)

			# Insert image array and shape with corresponding ID to DB (queue)
			image_shape = list(image.shape)
			image = image.copy(order='C')
			k = str(uuid.uuid4())
			image = helpers.base64_encode_image(image)
			d = {'id': k, 'image': image, 'image_height': image_shape[0], 'image_width': image_shape[1], 'image_channel': image_shape[2]}
			db.rpush(config.queue_sys.image_queue, json.dumps(d))

			# Retrieve results from db (post object-detection)
			while True:
				output = db.get(k)
				if output is not None:
					output = output.decode('utf-8')
					data['detections'] = json.loads(output)
					db.delete(k)
					break 
				time.sleep(config.queue_sys.client_sleep)
			data['success'] = True 
		except Exception as e:
			print(e)
			data['error_message'] = e 

	# Return response to client
	return flask.jsonify(data)

if __name__ == '__main__':
	print("Starting Object Detection microservice")
	app.run()
	print('Object Detection microservice started')