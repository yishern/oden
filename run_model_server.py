from configs import config 
from luminoth.utils.config import get_config 
from predicting import RPNNetwork 
import redis 
import json 
import helpers 
import time
import sys 

db = redis.StrictRedis(**config.redis)

def set_config(config_file):
	config = get_config(config_file)
	config.model.network.with_rcnn = False 
	return config 

def connect_db():
	try:
		# PING for Redis-server's connection
		db.ping()
	except:
		sys.exit('Failed to connect to Redis DB')

def load_model():
	# Instantiate model 
	rpn_config = set_config(config.model_config)
	global model 
	model = RPNNetwork(rpn_config)
	print('Model instantiated')


def classify_process():
	while True:
		queue = db.lrange(config.queue_sys.image_queue, 0,0)
		for q in queue:
			q = json.loads(q.decode('utf-8'))
			image_shape = (q['image_height'], q['image_width'], q['image_channel'])
			image = helpers.base64_decode_image(q['image'], image_shape)
			objects = model.predict_image(image)
			db.set(q['id'], json.dumps(objects))
			db.ltrim(config.queue_sys.image_queue, 1, -1)
			# time.sleep(config.queue_sys.server_sleep)

if __name__ == '__main__':
	print('Starting model server now')

	print('Inspecting connection to Redis DB')
	connect_db()

	# Instantiate model globally 
	load_model()

	# Begin listening to request and perform object detection
	classify_process()
	print('Model server loaded')
	
